const express = require('express');
const path = require('path')
const app = express();
const port = 3000;

// serve static files in dist and images folder
app.use('/dist', express.static(path.join(__dirname, 'public/dist')))
app.use('/images', express.static(path.join(__dirname, 'public/images')))

app.get('/', (req, res) => {
    res.header('Cache-Control', 'max-age=60, must-revalidate, private');
    res.sendFile(path.join(__dirname, 'public/index.html'));
})

app.listen(port, () => {
    console.log(`Examplge app listening on port ${port}`)
})